import Vue from "vue";
import App from "./App.vue";
import VueAnalytics from "vue-analytics";
import "./registerServiceWorker";
import router from "./router";

Vue.config.productionTip = false;

Vue.use(VueAnalytics, {
  id: process.env.VUE_APP_GOOGLE_ANALYTICS,
  router
});

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
